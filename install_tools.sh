#!/bin/bash
apt update
# Common utilities
apt-get install -y  apt-transport-https software-properties-common git

# Ansible installation
apt-add-repository --yes --update ppa:ansible/ansible
apt install  -y ansible

# VIM  from playbook installation  for ansible Development
ansible-playbook ansible-vim-config/install_custom_vim.yml
